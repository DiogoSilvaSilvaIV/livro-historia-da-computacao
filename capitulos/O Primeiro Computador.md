# O Primeiro Computador do mundo

### **Surgimento:**

Há exatos 65 anos, em  14 de fevereiro de 19466, os pesquisadores norte-americanos John Eckert e John Mauchly, da Electronic Control Company, revelavam ao mundo o primeiro computador eletrônico digital de larga escala, também conhecido como Electronic Numerical Integrator And Computer (Computador e Integrador Numérico Eletrônico) ou apenas ENIACs. Para esclarecer, ele foi usado para fins gerais, como uma éspecie de calculadora para solução de problemas numéricos.

Desenvolvido a pedido do exército dos Estados Unidos para seu laboratório de pesquisa balística, o ENIAC era um monstro de 30 toneladas de peso que ocupava uma área de 180 m² de área construída. Sua produção custou nada menos do que US$ 500 mil na época, o que hoje representaria aproximadamente US$ 6 milhões e a máquina contava com um hardware equipado com 70 mil resístores e 18 mil válvulas de vácuo que em funcionamento consumiam vorazmente 200 mil watts de energia.

### **Sistema Operacional e afins:**

Seu “sistema operacional” consistia em cartões perfurados operados por um time de funcionárias do exército, o que de quebra as classifica como as primeiras programadoras que se tem notícia. Sua construção de iniciou em plena guerra, em 1943. Apesar mostrado em 1946, só foi ser ligado pela primeira vez em julho de 47.

### **Funcionamento:**

O que distinguia o ENIAC dos dispositivos existentes da época era que, apesar de funcionar em velocidades eletrônicas, ele também podia ser programado para atender a diferentes instruções. Porém, eram necessários vários dias para religar a máquina com novas instruções, mas apesar de todo o trabalho para operá-lo, não havia como negar que o ENIAC foi o primeiro computador eletrônico de uso geral do mundo.

### **Operações:**

Em 14 de fevereiro de 1946, o primeiro computador da história foi anunciado ao público pelo Departamento de Guerra dos Estados Unidos. Inclusive, um dos primeiros comandos que a máquina executou, foram cálculos para a construção de uma bomba de hidrogênio. Neste sentido, o ENIAC levou apenas 20 segundos e foi verificado em relação a uma resposta obtida após quarenta horas de trabalho com uma calculadora mecânica.
Além desta operação, o primeiro computador inventado fez vários outros cálculos como por exemplo:

**1- Previsão do tempo;**

**2- Cálculos de energia atômica;**

**3- Ignição térmica;**

**4- Projetos de túneis de vento;**

**5- Estudos de raios cósmicos;**

**6- Cálculos usando números aleatórios;**

**7- Estudos científicos.**

### **Conclusão:**

O primeiro computador já inventado foi, sem dúvida, a origem da indústria de computadores comerciais tanto dos Estados Unidos como de todo o mundo. Entretanto, seus inventores, Mauchly e Eckert, nunca alcançaram fortuna com seu trabalho e a empresa da dupla afundou em diversos problemas financeiros, até ser vendida por um preço abaixo do que realmente valia. Em 1955, a IBM vendia mais computadores do que o UNIVAC e, na década de 1960, o grupo de oito empresas que vendiam computadores era conhecido como “IBM e os sete anões”.
Por fim, a IBM cresceu tanto que o governo federal moveu vários processos contra ela de 1969 a 1982. Além disso, foi a IBM, a primeira empresa a contratar a desconhecida, mas agressiva, Microsoft para fornecer o software para seu computador pessoal. Ou seja, este lucrativo contrato permitiu que a Microsoft se tornasse tão dominante e permanecesse atuando no ramo da tecnologia e lucrando nele até os dias atuais.

# Referências
LIMA, A. Primeiro computador - Origem e história do famoso ENIAC. Disponível em: <https://segredosdomundo.r7.com/primeiro-computador-eniac/>. Acesso em: 18 maio. 2022.

‌JOÃO BRUNELLI MORENO. A história do ENIAC, o primeiro computador do mundo – Tecnoblog. Disponível em: <https://tecnoblog.net/especiais/eniac-primeiro-computador-do-mundo-completa-65-anos/>. Acesso em: 18 maio. 2022.

‌
