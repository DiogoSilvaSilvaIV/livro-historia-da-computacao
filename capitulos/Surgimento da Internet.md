# O surgimento da Internet

Conhecer a história da Internet é tão útil quanto fascinante. Neste capítulo, você descobrirá os principais acontecimentos 
que marcaram o nascimento da mais importante revolução tecnológica do mundo nos últimos 40 anos.

## Origem

A rede mundial de computadores, ou Internet, surgiu em plena Guerra Fria. Criada com objetivos militares, seria uma das 
formas das forças armadas norte-americanas de manter as comunicações em caso de ataques inimigos que destruíssem os meios 
convencionais de telecomunicações. Nas décadas de 1970 e 1980, além de ser utilizada para fins militares, a Internet também 
foi um importante meio de comunicação acadêmico. Estudantes e professores universitários, principalmente dos EUA, trocavam 
ideias, mensagens e descobertas pelas linhas da rede mundial.

## Desenvolvimento da Internet 

Foi somente no ano de 1990 que a Internet começou a alcançar a população em geral. Neste ano, o engenheiro inglês Tim 
Bernes-Lee desenvolveu a World Wide Web, possibilitando a utilização de uma interface gráfica e a criação de sites mais 
dinâmicos e visualmente interessantes. A partir deste momento, a Internet cresceu em ritmo acelerado. Muitos dizem que foi 
a maior criação tecnológica, depois da televisão na década de 1950.

A década de 1990 tornou-se a era de expansão da Internet. Para facilitar a navegação pela Internet, surgiram vários 
navegadores (browsers) como, por exemplo, o Internet Explorer da Microsoft e o Netscape Navigator. O surgimento acelerado 
de provedores de acesso e portais de serviços on line contribuiu para este crescimento. A Internet passou a ser utilizada 
por vários segmentos sociais. Os estudantes passaram a buscas informações para pesquisas escolares, enquanto jovens 
utilizavam para a pura diversão em sites de games. As salas de chat tornaram-se pontos de encontro para um bate-papo 
virtual a qualquer momento. Desempregados iniciaram a busca de empregos através de sites de agências de empregos ou 
enviando currículos por e-mail. As empresas descobriram na Internet um excelente caminho para melhorar seus lucros e as 
vendas on line dispararam, transformando a Internet em verdadeiros shoppings centers virtuais.

## Internet nos dias atuais

Nos dias atuais, é impossível pensar no mundo sem a Internet. Ela tomou parte dos lares de pessoas do mundo todo. Estar 
conectado a rede mundial passou a ser uma necessidade de extrema importância. A Internet também está presente nas escolas, 
faculdades, empresas e diversos locais, possibilitando acesso às informações e notícias do mundo em apenas um click.

## A febre das redes sociais

A partir de 2006, começou uma nova era na Internet com o avanço das redes sociais. Pioneiro, o Orkut ganhou a preferência 
dos brasileiros. Nos anos seguintes surgiram outras redes sociais como, por exemplo, Facebook, Twitter, Google Plus e 
Instagram.

## Os sites de compras coletivas

A partir de 2010, um novo serviço virou febre no mundo da Internet. Conhecidos como sites de compras coletivas, eles fazem 
a intermediação entre consumidores e empresas. Estes sites conseguem negociar descontos para a venda de grande quantidade 
de produtos e serviços. Os consumidores compram cupons com 50% de desconto ou até mais. Os sites que mais se destacam neste 
segmento são: Peixe Urbano e Groupon.

## Marco regulatório da Internet no Brasil

Em 2014, foi aprovado (no Congresso Nacional e Senado) e sancionado pela presidente Dilma Rousseff, o Marco Civil da 
Internet, após longo período de debates e tramitação. Um dos principais pontos da lei é a implantação no Brasil do 
princípio da "neutralidade da rede". Esta lei proíbe as empresas que oferecem acesso à rede (operadoras de telefonia, por 
exemplo) de cobrarem pelo tipo de conteúdo que o internauta (assinante) acessa.

# Refêrencias 

História da Internet - origem e história resumida - Sua Pesquisa. Disponível em: <https://www.suapesquisa.com/internet/>. 
Acesso em: 18 maio. 2022.

EQUIPE BRASIL ESCOLA. História da Internet, Funcionamento da Internet - Browsers. Disponível em: <https://brasilescola.uol.com.br/informatica/internet.htm>. Acesso em: 18 maio. 2022.

‌DOS, C. história da Internet, um sistema global de redes de computadores interconectadas. Disponível em: <https://pt.wikipedia.org/wiki/Hist%C3%B3ria_da_Internet>. Acesso em: 18 maio. 2022.

‌
‌
