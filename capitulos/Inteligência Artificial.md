# Evolução da Inteligência Artificial no mundo

### **Como surgiu a inteligência artifical?**

Os principais acontecimentos que levaram ao surgimento da Inteligência Artificial começaram na década de 1950. Nessa época, o cientista Alan Turing desenvolveu o chamado Teste de Turing.

O objetivo era testar o potencial de uma máquina para imitar o pensamento e comportamento humano.

O Teste de Turing questiona: Uma máquina consegue imitar o comportamento humano?

E foi aí que surgiu o Jogo da Imitação, composto por 3 jogadores.

Jogador A é uma máquina, que deve convencer o interrogador de que é, na verdade, um humano;
Jogador B é um ser humano qualquer, que deve ajudar o interrogador dando respostas verdadeiras;
Jogador C, o interrogador, deve ainda descobrir quem é quem.
Temos, agora, o Teste de Turing: um teste capaz de concluir se um computador (jogador A) é inteligente. Conseguindo enganar o
interrogador e convencê-lo de que é humana, a máquina poderia ser considerada inteligente.

O teste proposto traz uma das ideias mais primitivas acerca do que é inteligência artificial (IA), objeto de estudo do Grupo 
Turing. Trata-se um algoritmo conversacional, baseado em uma área específica dentro do campo de IA, chamada de Processamento de
Linguagem Natural. O nome faz bastante sentido — o computador deve entender exatamente o que está sendo demandado dele, mesmo
sem instruções diretamente programadas, e responder de forma a imitar a linguagem natural de uma conversação humana.

A partir desse teste, muitos cientistas ficaram motivados a pesquisar maneiras dos computadores agir mais naturalmente em
comparação a uma pessoa. 

Em 1956, aconteceu a Conferência de Dartmouth que classificou o desenvolvimento de máquinas inteligentes como uma ciência e
foi nesse evento que surgiu o termo Inteligência Artificial. 

Nos anos 90, o destaque foi para o supercomputador da IBM, chamado Deep Blue, que venceu no xadrez o jogador Garry Kasparov,
considerado o maior enxadrista de todos os tempos. 

A Inteligência Artificial não parou de evoluir e se tornou uma parte essencial do modo de viver da atualidade. 


### **Qual a evolução da Inteligência Artificial nos últimos anos?**

Por meio da utilização de algoritmos inteligentes, as máquinas sofreram melhorias, começaram a aprender com os dados e se tornaram capazes de tomar decisões. 

Dessa forma, os programas de computadores deixaram de ser apenas um item dependente do ser humano para o bom funcionamento, mas começaram a ter ações próprias que facilitam as atividades rotineiras do ser humano. 

Atualmente, são inúmeros softwares que usam códigos baseados nos princípios da Inteligência Artificial, por exemplo: 

Google: a ferramenta de pesquisa mais popular do mundo é uma forma de IA, é possível perceber isso tanto nas sugestões de pesquisa para o usuário como nos resultados encontrados, que recebem uma posição conforme o cumprimento dos critérios analisados pela Inteligência Artificial;    

Assistentes virtuais:  as máquinas possuem um sistema de linguagem próprio mas, hoje em dia, os softwares conseguem compreender o comando de voz humano e executar tarefas, esse é o caso da Alexa e Siri.

Além disso, o avanço da IA também afetou positivamente as empresas, possibilitando a automatização de processos de modo eficiente, o que aumenta a produtividade e otimiza o tempo. 

A evolução da Inteligência Artificial é um processo constante, a tecnologia nunca para de progredir.

#### **Avanços esperados para a inteligência artificial em 2022:**

**1- Expansão da criatividade;**

**2- Aumento de comandos por voz;**

**3- Melhor compreensão da linguagem.**

#  Referências 

1- Evolução da Inteligência Artificial em 2021: melhorias e tendências - People. Disponível em: <https://people.com.ai/evolucao-da-inteligencia-artificial/#:~:text=Os%20principais%20acontecimentos%20que%20levaram,o%20pensamento%20e%20comportamento%20humano.>. Acesso em: 18 maio. 2022.


2- MAGALDI, R. O que é o Teste de Turing?  - Turing Talks - Medium. Disponível em: <https://medium.com/turing-talks/turing-talks-1-o-que-%C3%A9-o-teste-de-turing-ee656ced7b6>. Acesso em: 18 maio. 2022.

‌









