# **Criação do disco Rigido**

## Sobre o disco Rigido
Um disco rígido é um hardware usado para armazenar conteúdo digital e dados em computadores. Cada computador tem um disco rígido interno, mas você também pode obter discos rígidos externos que podem ser usados para expandir o armazenamento de um computador.
O tipo mais “tradicional” de disco rígido é o HDD.

As unidades de disco rígido são compostas de discos magnetizados, conhecidos como pratos, que giram rapidamente, normalmente entre 5.400 e 15.000 RPM. Quanto mais rápido o disco magnético gira, mais rápido seu computador é capaz de acessar informações a partir dele.
Todos os dados digitais vêm na forma de código binário, uma série de uns e zeros que podem representar qualquer informação. Os cabeçotes de leitura/gravação de um disco rígido são usados para inserir esses uns e zeros por magnetização de partes do prato. Cada pequena porção do prato abriga um bit, que será igual a 1 ou 0. O cabeçote pode detectar o magnetismo de cada porção, “lendo” informações. O mesmo cabeçote que pode “ler” dados também pode “gravar”, alterando a magnetização de bits em um prato.

Mas, é claro, existem alguns pontos negativos. Os discos rígidos podem ser lentos, principalmente para abrir aplicativos ou arquivos grandes. Como eles não gravam dados sequencialmente, os dados são fragmentados, ou seja, existe um espaço vazio entre cada compartimento. Esses espaços vazios são muito pequenos para se armazenar dados, mas quando somados, podem ocupar uma parte considerável do disco.

Sempre que uma alteração for feita, como um novo arquivo sendo salvo ou um arquivo sendo excluído, o cabeçote do disco rígido ajustará o magnetismo do prato de acordo. Você pode imaginá-lo como um toca-discos, com o disco de vinil sendo o prato contendo a informação, e o braço sendo os cabeçotes que escaneiam essa informação.
Como os dados são armazenados magneticamente, os HDDs são dispositivos não voláteis, o que significa que retêm dados mesmo quando o computador está desligado.

O prato e o braço de leitura são componentes delicados, por isso são envoltos por uma caixa de aço. Isso impede que o disco seja danificado, em circunstâncias normais.

## Breve História do disco rígido

### O Primeiro disco Rígido 

Depois de experimentar com fita magnética como um meio de armazenamento de dados, o primeiro disco rígido comercial foi projetado em 1956 por uma equipe da IBM liderada por Reynold B. Johnson.
 equipe da IBM descobriu que eles poderiam armazenar dados em discos de metal magnetizado que poderiam ser substituídos por novas informações, o que levou à construção do primeiro sistema de disco rígido, conhecido como RAMAC (Random Access Method of Accounting and Control).

 O disco rígido original tinha aproximadamente o tamanho de dois frigoríficos, com um total de 50 pratos de 24 polegadas girando a 1.200 RPM.Apesar de seu tamanho, o RAMAC tinha uma capacidade de armazenamento de apenas 5 MB, aproximadamente o tamanho de uma imagem e, apesar de sua capacidade, custou cerca de US$ 10.000 por megabyte.

 ### Discos subsequentes

 Os RAMACs foram alojados em centros de processamento de dados até que a IBM introduziu o armazenamento removível na década de 1960. O IBM 1311 Disk Storage Drive de 1962 tinha 2,6 MB em seis pratos de 14 polegadas. Eles eram aproximadamente do tamanho de uma máquina de lavar louça.

 Computadores pessoais emergiram nos anos 70 e, ao mesmo tempo, a IBM estava desenvolvendo os primeiros disquetes. Lançado pela primeira vez em 1971, o disquete foi o primeiro disco magnético facilmente portátil. Você pode considerá-lo o primeiro disco rígido externo. Os disquetes se tornaram o padrão para armazenamento em disco até que CDs graváveis e as unidades flash USB se tornaram prevalentes por volta da virada do século. O primeiro disco rígido de leitura/gravação para computadores pessoais foi lançado em 1972 pela Memorex.
 
 Pouco tempo depois, em 1973, a IBM lançou um HD que armazenava 10 Mega bytes, o 30/30 Winchester, que custava em media dois mil Dólares. Atualmente um HD de 1.5 TB custa menos de 80 dólares.

 Em 1980, muitas grandes empresas passaram a trabalhar com o HDD, e a unidade ST-506 da Shugart Technology tornou-se o disco rígido mais compacto disponível na época, com 5,25 polegadas com uma capacidade de 5 MB. A IBM, entretanto, lançou o IBM 3380, que foi o primeiro disco rígido a oferecer 1 GB de armazenamento.

Em 1983, a Rodime lançou o RO352, o primeiro disco rígido de 3,5 polegadas, com dois pratos e uma capacidade de 10 MB. HDDs de 3,5 polegadas logo se tornariam o padrão de computadores desktop, e ainda são usados até hoje (com HDDs de notebook tendo 2,5 polegadas).

Foi nos anos 80 que os discos rígidos externos com os quais estamos familiarizados hoje começaram a tomar forma e, com o tempo, à medida que o tamanho físico dos discos rígidos externos diminuía, a capacidade do disco rígido aumentava.

## Outras formas de armazenamento

### O SSD

 Os discos de estado sólido (SSDs) são discos de armazenamento que têm as mesmas funções dos discos rígidos. Os SSDs usam uma tecnologia diferente da dos HDDs, mais parecida com a tecnologia de um pendrive. Essa tecnologia consome menos energia e produz menos calor. Além disso, os SSDs não têm peças mecânicas, por isso duram mais em computadores portáteis.
 ### Nuvem

  chegada do armazenamento em nuvem apresentou uma solução para as limitações e riscos dos discos rígidos, oferecendo uma alternativa de armazenamento de dados mais segura e acessível. Salvar um arquivo na nuvem significa armazená-lo on-line, onde ele não ocupará espaço em seu dispositivo.

  ## | Referencias |

  O que é disco rígido e para que serve? Disponível em: <https://experience.dropbox.com/pt-br/resources/what-is-a-hard-drive#:~:text=Um%20breve%20hist%C3%B3rico%20da%20unidade,Johnson.>. Acesso em: 18 maio. 2022.

‌O que é um disco rígido? Disponível em: <https://br.crucial.com/articles/pc-builders/what-is-a-hard-drive>. Acesso em: 18 maio. 2022.

‌O que é HD (disco rígido): características e como funciona — InfoWester. Disponível em: <https://www.infowester.com/hd.php>. Acesso em: 18 maio. 2022.

‌
