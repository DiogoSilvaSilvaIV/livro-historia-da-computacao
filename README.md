# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [A criação do disco rigido](https://gitlab.com/DiogoSilvaSilvaIV/livro-historia-da-computacao/-/blob/master/capitulos/Criação%20do%20Disco%20Rígido.md)
1. [Inteligência Artificial](https://gitlab.com/DiogoSilvaSilvaIV/livro-historia-da-computacao/-/blob/master/capitulos/Inteligência%20Artificial.md)
1. [O primeiro computador do mundo](https://gitlab.com/DiogoSilvaSilvaIV/livro-historia-da-computacao/-/blob/master/capitulos/O%20Primeiro%20Computador.md)
1. [Surgimento da Internet](https://gitlab.com/DiogoSilvaSilvaIV/livro-historia-da-computacao/-/blob/master/capitulos/Surgimento%20da%20Internet.md)




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11364529/avatar.png?width=400)  | Diogo Silva da Silva | DiogoSilvaSilvaIV | [silva.diogoda@gmail.com](mailto:silva.diogoda@gmail.com)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11253127/avatar.png?width=400)  | Lorenzo Benetti | Benetti | [lb.benetti@hotmail.com](mailto:lb.benetti@hotmail.com)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11329935/avatar.png?width=400)  | Pedro Henrique Camara | Gaudério | [pedrohenriquecamara@outlook.com.br](mailto:pedrohenriquecamara@outlook.com.br)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/11385060/avatar.png?width=400)  | Matheus Dall olmo | matheusdll | [matheusdll@outlook.com.br](mailto:matheusdll@outlook.com.br)
